// Fill out your copyright notice in the Description page of Project Settings.

#include "Multiplayer/WGameMode.h"
#include "Player/WPlayerController.h"
#include "Player/WCharacter.h"

AWGameMode::AWGameMode()
{
	PlayerControllerClass = AWPlayerController::StaticClass();
}

UClass* AWGameMode::GetDefaultPawnClassForController(AController* InController)
{
	AWPlayerController* PlayerController = Cast<AWPlayerController>(InController);
	if (PlayerController)
	{
		return DefaultCharacterClass;
	}
	else
	{
		return DefaultPawnClass;
	}
}
