// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "WCharacter.generated.h"

class UCameraComponent;

UCLASS()
class WARFARE_API AWCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	/** Default UObject constructor */
	AWCharacter();

private:
	/** Arms mesh: 1st person view (arms; seen only by self) */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category=Character, meta=(AllowPrivateAccess="true"))
	TObjectPtr<USkeletalMeshComponent> ArmsMesh;

	/** First person camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category=Character, meta=(AllowPrivateAccess="true"))
	TObjectPtr<UCameraComponent> FirstPersonCameraComponent;

public:
	/** Returns ArmsMesh subobject */
	FORCEINLINE USkeletalMeshComponent* GetArmsMesh() const {return ArmsMesh;}

	/** Returns FirstPersonCameraComponent subobject */
	FORCEINLINE UCameraComponent* GetFirstPersonCameraComponent() const {return FirstPersonCameraComponent;}

	/** Handles moving forward */
	virtual void MoveForward(float Value);

	/** Handles strafing movement, left and right */
	virtual void StrafeRight(float Value);
};
