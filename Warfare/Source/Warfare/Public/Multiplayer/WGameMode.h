// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameMode.h"
#include "WGameMode.generated.h"

class AWCharacter;

UCLASS()
class WARFARE_API AWGameMode : public AGameMode
{
	GENERATED_BODY()
	
public:
	AWGameMode();

	/** The default character class used by players. */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category=Classes)
	TSubclassOf<AWCharacter> DefaultCharacterClass;

	UClass* GetDefaultPawnClassForController(AController* InController);
};
